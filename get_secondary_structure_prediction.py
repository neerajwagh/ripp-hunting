#!/usr/bin/env python
# coding: utf-8

# In[75]:


import pandas as pd
import subprocess
import os
import pickle 


# In[66]:


def read_ss2_output():
    temp_file = "input.ss2"
    psipred_df = pd.read_csv(temp_file, sep="  ", header=None, skiprows = 2)
    ss_seq = ''.join([x.split(' ')[-1] for x in psipred_df[0]])
    return (ss_seq)


# In[67]:


file = "./training_data_clean_small.csv"
df = pd.read_csv(file)
df.drop_duplicates(subset=["aa_seq"], keep="first", inplace=True)
df.shape


# In[71]:


# df.head()


# In[74]:


# df.iloc[2]["aa_seq"]


# In[68]:


col = df["aa_seq"]
ss_seq_list = []
temp_file = "input.temp"

os.chdir('/mnt/c/Users/neera/OneDrive/Documents/CS598SS/final_project/psipred')

for i in range(df.shape[0]):
    
    # if i == 100:
    #     break
    
    aa_seq = df.iloc[i]["aa_seq"]
    
    # create temp file for single input    
    with open(temp_file, 'w') as fp:
        fp.write(aa_seq)
    
    # runpsipred_single
    process = subprocess.Popen(['./runpsipred_single', 
                                temp_file],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    
    # read output
    ss_seq = read_ss2_output()
    
    # add to list
    ss_seq_list.append(ss_seq)
    
    print ("done!", i)
   
    if (i % 500 == 0):
        print ("checkpointing!", i)
        with open('ss_seq_list_'+str(i)+".pkl", 'wb') as fp: 
            pickle.dump(ss_seq_list, fp, pickle.HIGHEST_PROTOCOL)
    
# clean temp files
# subprocess.Popen(['rm *.ss2 *.horiz *.ss'])

# add list to dataframe as a column
with open('ss_seq_list_full.pkl', 'wb') as fp: 
    pickle.dump(ss_seq_list, fp, pickle.HIGHEST_PROTOCOL)
df['ss_seq'] = ss_seq_list

# save new csv to disk
os.chdir('/mnt/c/Users/neera/OneDrive/Documents/CS598SS/final_project')
df.to_csv("dataset_small.csv", index=False, header=True)

print ("dataset_small.csv saved!")

